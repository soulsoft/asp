macro package asp.macros

import std.ast.*
import std.ast.*
import std.collection.*
import std.sort.*

public macro Inject(tokens: Tokens): Tokens {
    let classDecl = ClassDecl(tokens)
    let constractors = classDecl.body.decls |> filter {f => f is FuncDecl && f.keyword.value == "init"} |>
        map {a => (a as FuncDecl).getOrThrow()} |> collectArray
    constractors.sortBy(stable: true) {
        a, b =>
        if (a.funcParams.size > b.funcParams.size) {
            Ordering.LT
        } else if (a.funcParams.size < b.funcParams.size) {
            Ordering.GT
        }
        Ordering.GT
    }
    if (let Some(constractor) <- (constractors |> first)) {
        let params = Tokens()
        for ((index, pattern) in constractor.funcParams |> enumerate) {
            params.append(quote(sp.getOrThrow<$(pattern.paramType.toTokens())>()))
            if (index + 1 < constractor.funcParams.size) {
                params.append(quote(,))
            }
        }
        let funcDecl = FuncDecl(
            quote(
                public static func apply(sp :ServiceProviderEngine) {
            $(classDecl.identifier)($(params))
        })
        )
        classDecl.body.decls.add(funcDecl)
    } else {
        let funcDecl = FuncDecl(
            quote(public static func apply(sp :ServiceProviderEngine) {
            $(classDecl.identifier)()
        }))
        classDecl.body.decls.add(funcDecl)
    }
    classDecl.toTokens()
}
