package asp.hosting.http

import net.http.*
import std.reflect.*
import std.collection.*
import asp.extensions.injection.*

private let threadLocal = ThreadLocal<HashMap<TypeInfo, Any>>()

private func getThreadLocal(key: TypeInfo): ?Any {
    if (let Some(props) <- threadLocal.get() && props.contains(key)) {
        return props[key]
    }
    return None
}

private func setThreadLocal(key: TypeInfo, value: Any) {
    if (threadLocal.get().isNone()) {
        threadLocal.set(HashMap<TypeInfo, Any>())
    }
    if (let Some(props) <- threadLocal.get()) {
        props[key] = value
    } else {
        threadLocal.set(HashMap<TypeInfo, Any>((key, value)))
    }
}

private func resetThreadLocal() {
    threadLocal.set(HashMap<TypeInfo, Any>())
}

public interface IHttpContextExtensions {
    func set(key: TypeInfo, value: Any): Unit
    func get(key: TypeInfo): ?Any
    func reset(): Unit
    func setEndpoint(endpoint: Endpoint): Unit
    func getEndpoint(): ?Endpoint
    prop services: ServiceProvider
}

extend HttpContext <: IHttpContextExtensions {
    public func set(key: TypeInfo, value: Any) {
        setThreadLocal(key, value)
    }

    public func get(key: TypeInfo): ?Any {
        getThreadLocal(key)
    }

    public func setEndpoint(endpoint: Endpoint) {
        set(TypeInfo.of<Endpoint>(), endpoint)
    }

    public func reset() {
        resetThreadLocal()
    }

    public func getEndpoint(): ?Endpoint {
        if (let Some(obj) <- get(TypeInfo.of<Endpoint>())) {
            return (obj as Endpoint)
        }
        return None
    }

    public prop services: ServiceProvider {
        get() {
            return (get(TypeInfo.of<ServiceProvider>()).getOrThrow() as ServiceProvider).getOrThrow()
        }
    }
}
