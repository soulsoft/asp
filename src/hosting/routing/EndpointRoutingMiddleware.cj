package asp.hosting.routing

import net.http.*
import asp.hosting.*
import std.collection.*
import asp.hosting.mvc.*
import asp.hosting.http.*
import asp.extensions.injection.*

public class EndpointRoutingMiddleware <: IMiddleware {
    private let _router: IRouter

    public init(router: IRouter, endpointDataSource: IEndpointDataSource) {
        _router = router
        _router.setup(endpointDataSource.endpoints)
    }

    public func invoke(context: HttpContext, netxt: () -> Unit): Unit {
        let entpoints = _router.route(context)
        let method = context.request.method.toAsciiLower()
        if (entpoints.size > 0) {
            if (let Some(endpoint) <- (entpoints |> filter {item => isAllowMethod(method, item)} |> first)) {
                context.setEndpoint(endpoint)
                netxt()
            } else {
                context.responseBuilder.status(405)
            }
        } else {
            netxt()
        }
    }

    private func isAllowMethod(method: String, endpoint: Endpoint) {
        if (HttpMethods.isOptions(method)) {
            return true
        }
        if (HttpMethods.isGet(method)) {
            return endpoint.metadata |> any {item => item is HttpGet}
        }
        if (HttpMethods.isPut(method)) {
            return endpoint.metadata |> any {item => item is HttpPut}
        }
        if (HttpMethods.isHead(method)) {
            return endpoint.metadata |> any {item => item is HttpHead}
        }
        if (HttpMethods.isPost(method)) {
            return endpoint.metadata |> any {item => item is HttpPost}
        }
        if (HttpMethods.isPatch(method)) {
            return endpoint.metadata |> any {item => item is HttpPatch}
        }
        if (HttpMethods.isDelete(method)) {
            return endpoint.metadata |> any {item => item is HttpDelete}
        }
        return false
    }

    public static func apply(sp: ServiceProvider) {
        EndpointRoutingMiddleware(sp.getOrThrow<IRouter>(), sp.getOrThrow<IEndpointDataSource>())
    }
}
