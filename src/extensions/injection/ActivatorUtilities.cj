package asp.extensions.injection

import std.reflect.*
import std.collection.*

public class ActivatorUtilties {
    
    public static func createInstance<T>(sp: IServiceProvider, args: Array<Any>) :T {
        let obj = createInstance(TypeInfo.of<T>(), sp, args) as T
        return obj.getOrThrow()
    }

    public static func createInstance(typeInfo: TypeInfo, sp: IServiceProvider, args: Array<Any>) {
        var argIndex = 0
        let constructor = getInstanceConstructor(typeInfo)
        let parameters = ArrayList<Any>()
        for (pattern in constructor.parameters) {
            if (let Some(parameter) <- sp.getOrDefault(pattern.typeInfo)) {
                parameters.add(parameter)
            } else {
                if (args.size <= argIndex) {
                    throw Exception(
                        "Argument at index ${argIndex} ('${pattern.name}') not found for type '${typeInfo}' using constructor '${constructor}'."
                    )
                }
                let arg = args[argIndex]
                parameters.add(arg)
                argIndex += 1
            }
        }
        return (constructor
            .apply(parameters |> collectArray) as Object)
            .getOrThrow {
                Exception("Failed to create instance of type '${typeInfo}' using constructor '${constructor}'.")
            }
    }

    static func getInstanceConstructor(typeInfo: TypeInfo) {
        if (let Some(classInfo) <- (typeInfo as ClassTypeInfo)) {
            let list = classInfo.constructors |> collectArrayList
            list.sortBy {
                a, b => if (a.parameters.size > b.parameters.size) {
                    Ordering.LT
                } else if (a.parameters.size < b.parameters.size) {
                    Ordering.GT
                } else {
                    Ordering.EQ
                }
            }
            return (list |> first).getOrThrow {
                Exception("A suitable constructor for type '${typeInfo}' could not be located. Ensure the type is concrete and services are registered for all parameters of a public constructor.")
            }
        }
        throw Exception("The type '${typeInfo}' is not a class type.")
    }
}
