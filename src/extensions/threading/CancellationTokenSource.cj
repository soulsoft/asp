package asp.extensions.threading

import std.sync.Timer
import std.collection.*

public open class CancellationTokenSource {
    private var _isCancellationRequested: Bool = false
    private var _callbacks = ArrayList<() -> Unit>()
    /*
    获取一个新的令牌
     */
    public prop token: CancellationToken {
        get() {
            CancellationToken(this)
        }
    }

    // 请求取消
    public func cancel() {
        cancelAfter(Duration.Zero)
    }

    public func cancelAfter(delay: Duration) {
        notifyCancellation(delay)
    }

    // 通知所有注册的回调
    private func notifyCancellation(delay: Duration): Unit {
        if (_isCancellationRequested) {
            return
        }
        Timer.once(delay) {
            _isCancellationRequested = true
            for (callback in _callbacks) {
                callback()
            }
            _callbacks.clear()
        }
    }

    func register(callback: () -> Unit) {
        if (_isCancellationRequested) {
            callback() // 已取消，立即执行回调
        } else {
            _callbacks.add(callback)
        }
    }

    public prop isCancellationRequested: Bool {
        get() {
            _isCancellationRequested
        }
    }

    public static func createLinkedTokenSource(tokens: Array<CancellationToken>): CancellationTokenSource {
        // 创建一个新的链接取消令牌源
        let cts = LinkedCancellationTokenSource(tokens)
        for (token in tokens) {
            token.register {
                cts.cancel()
            }
        }
        return cts
    }
}

class LinkedCancellationTokenSource <: CancellationTokenSource {
    private let _tokens: Array<CancellationToken>

    init(tokens: Array<CancellationToken>) {
        _tokens = tokens
    }
}
