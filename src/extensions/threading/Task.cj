package asp.extensions.threading

/*
如果是频繁的创建Task场景下建议使用原生的线程表达式
 */
public class Task<T> {
    private var _result: ?T = None
    private var _action: ?() -> T
    private var _future: ?Future<T> = None
    private var _status: TaskStatus = TaskStatus.created
    private var _token: ?CancellationToken = None
    private var _exception: ?Exception = None

    private init() {
        _action = None
    }

    public init(action: () -> T) {
        _action = action
        setStatus(TaskStatus.waitingForActivation)
    }

    public func start(): Unit {
        start(None)
    }

    public func start(token: ?CancellationToken): Unit {
        if (!_status.isWaitingForActivation()) {
            return
        }
        if (let Some(action) <- _action) {
            setStatus(TaskStatus.waitingToRun)
            let future = spawn {
                let error: ?Exception
                try {
                    setStatus(TaskStatus.running)
                    let result = action()
                    return result
                } catch (ex: Exception) {
                    error = ex
                    setException(ex)
                    throw ex
                } finally {
                    if (let Some(_token) <- token && _token.isCancellationRequested) {
                        setStatus(TaskStatus.canceled)
                    } else if (error.isSome()) {
                        setStatus(TaskStatus.faulted)
                    } else {
                        setStatus(TaskStatus.ranToCompletion)
                    }
                }
            }
            _future = future
        } else {
            _status = TaskStatus.ranToCompletion
        }
    }

    public prop isCompleted: Bool {
        get() {
            match (_status) {
                case canceled => true
                case ranToCompletion => true
                case faulted => true
                case _ => false
            }
        }
    }

    public prop isCompletedSuccessfully: Bool {
        get() {
            match (_status) {
                case ranToCompletion => true
                case _ => false
            }
        }
    }

    public prop isCancel: Bool {
        get() {
            match (_status) {
                case canceled => true
                case _ => false
            }
        }
    }

    /*
    立即启动：一个没有取消能力的任务
     */
    public static func run(action: () -> T) {
        var task = Task<T>(action)
        task.start(None)
        return task
    }
    /*
    立即启动：运行一个带有取消能力的任务
     */
    public static func run(token: CancellationToken, action: () -> T) {
        var task = Task<T>(action)
        task.start(token)
        return task
    }

    public func wait(): ?T {
        if (let Some(result) <- _result) {
            return result
        }
        if (let Some(future) <- _future) {
            _result = future.get()
            return _result
        } else {
            return None
        }
    }

    public prop status: TaskStatus {
        get() {
            _status
        }
    }

    public prop id: Int64 {
        get() {
            return thread.id
        }
    }

    public prop name: String {
        get() {
            return thread.name
        }
    }

    public prop thread: Thread {
        get() {
            if (let Some(future) <- _future) {
                future.thread
            } else {
                Thread.currentThread
            }
        }
    }

    public static prop completedTask: Task<T> {
        get() {
            var task = Task<T>()
            task.start(None)
            return task
        }
    }

    private func setStatus(status: TaskStatus) {
        _status = status
    }

    private func setException(ex: ?Exception) {
        _exception = ex
    }
}

public enum TaskStatus <: ToString {
    created
    | waitingForActivation
    | waitingToRun
    | running
    | ranToCompletion
    | canceled
    | faulted

    func isWaitingForActivation() {
        match (this) {
            case waitingForActivation => true
            case _ => false
        }
    }

    public func toString() {
        match (this) {
            case created => "created"
            case waitingForActivation => "waitingForActivation"
            case waitingToRun => "waitingToRun"
            case running => "running"
            case ranToCompletion => "ranToCompletion"
            case canceled => "canceled"
            case faulted => "faulted"
        }
    }
}
