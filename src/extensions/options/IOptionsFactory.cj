package asp.extensions.options

import std.reflect.*
import std.collection.*
import std.sync.ReentrantMutex

public interface IOptionsFactory<TOptions> where TOptions <: Object {
    func create(name: String): TOptions
}

internal class OptionsFactory<TOptions> <: IOptionsFactory<TOptions> where TOptions <: Object {
    private let _lock = ReentrantMutex()
    private let _instances = HashMap<String, TOptions>()
    private let _configures: Array<IConfigureOptions<TOptions>>
    private let _configureAfters: Array<IConfigureAfterOptions<TOptions>>

    public init(configures: Array<IConfigureOptions<TOptions>>, configureAfters: Array<IConfigureAfterOptions<TOptions>>) {
        _configures = configures
        _configureAfters = configureAfters
    }

    public func create(name: String) {
        if (_instances.contains(name)) {
            return _instances[name]
        }
        synchronized(_lock) {
            if (_instances.contains(name)) {
                return _instances[name]
            }
            let instance = createInstance()
            for (pattern in _configures) {
                pattern.configure(name, instance)
            }
            for (pattern in _configureAfters) {
                pattern.configureAfter(name, instance)
            }
            return instance
        }
    }

    private func createInstance() {
        let typeInfo = TypeInfo.of<TOptions>()
        if (let classTypeInfo: ClassTypeInfo <- typeInfo) {
            if (let Some(contstructor) <- (classTypeInfo.constructors |> first)) {
                if (let obj: Object <- contstructor.apply()) {
                    if (let instance: TOptions <- obj) {
                        return instance
                    }
                }
                throw UnsupportedException("create fail")
            } else {
                throw UnsupportedException("not found contstructor")
            }
        } else {
            throw UnsupportedException("not class type")
        }
    }
}
