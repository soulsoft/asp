package asp.extensions.options

public interface IOptions<TOptions> where TOptions <: Object {
    prop value: TOptions
    func get(name: String): TOptions
}

public class OptionsWrapper<TOptions> <: IOptions<TOptions> where TOptions <: Object {
    private let _optionsFactory: IOptionsFactory<TOptions>

    init(optionsFactory: IOptionsFactory<TOptions>) {
        _optionsFactory = optionsFactory
    }

    public prop value: TOptions {
        get() {
            _optionsFactory.create(Options.defaultName)
        }
    }

    public func get(name: String): TOptions {
        _optionsFactory.create(name)
    }
}