package asp.extensions.configuration

import std.collection.*

class ConfigurationSection <: IConfigurationSection {
    private let _path: String
    private let _key: String
    private let _value: ?String
    private let _root: IConfigurationRoot

    init(path: String, key: String, value: ?String, root: IConfigurationRoot) {
        _path = path
        _key = key
        _value = value
        _root = root
    }

    public prop key: String {
        get() {
            _key
        }
    }

    public prop value: ?String {
        get() {
            _value
        }
    }

    public prop path: String {
        get() {
            _path
        }
    }

    public operator func [](key: String) {
        return _root[ConfigurationPath.combine(path, key)]
    }

    public func getValue(key: String) {
        return _root.getValue(ConfigurationPath.combine(path, key))
    }

    public func getSection(key: String): IConfigurationSection {
        return ConfigurationSection(ConfigurationPath.combine(path, key), key, this[key], _root)
    }

    public func getChildren(): Collection<IConfigurationSection> {
        let items = ArrayList<IConfigurationSection>()
        for (key in _root.getSectionKeys(_path)) {
            let section = getSection(key)
            items.add(section)
        }
        return items |> collectArray
    }
}
