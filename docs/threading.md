# 线程相关

## 取消令牌

* 方式1

``` cangjie
main(): Int64 {
    let cts = CancellationTokenSource()
    let token = cts.token
    token.register({ => println("cancel")})
    let fut = spawn {
        while (true) {
            sleep(Duration.second) 
            token.ThrowIfCancellationRequested()
            println("work ...")
        }
    }
    sleep(Duration.second * 10) 
    cts.cancel()
    return 0
}
```
* 方式2

``` cangjie
main(): Int64 {
    let cts = CancellationTokenSource()
    let token = cts.token
    let fut = spawn {
        while (true) {
            sleep(Duration.second) 
            if (token.isCancellationRequested) {
                println("cancelled")
                return
            }else {
                println("work ...")
            }
        }
    }
    sleep(Duration.second * 10) 
    cts.cancel()
    return 0
}
```
