# docker部署

1. 从官网下载linux版本的sdk
2. 创建一个目录：docker/cangjie
3. 把sdk解压，并将里面的cangjie/runtime和/cangjie/tools复制到/docker/cangjie(里面一些windows的文件可以删除掉)
4. 编译本项目，将产出文件main.exe放到docker下
5. 编写dockerfile文件

``` Dockerfile
# 使用 Ubuntu 作为基础镜像
FROM ubuntu:latest

# 暴露 80 端口
EXPOSE 80

# 创建目标目录
RUN mkdir -p /cangjie

# 复制本地的 cangjie 文件到容器中
COPY ./cangjie/ /cangjie

# 设置环境变量
#ENV CANGJIE_HOME=/cangjie
ENV LD_LIBRARY_PATH=/cangjie/runtime/lib/linux_x86_64_llvm:/cangjie/tools/lib

# PS:可以把上面的步骤制作成一个镜像，下面的步骤就基于上面制作出来的镜像继续制作

# 复制当前路径下的 main 文件到镜像中的 /root 目录
COPY main /app/

# 确保 main 文件是可执行的
RUN chmod +x /app/main

# 设置工作目录（可选）
WORKDIR /app

# 默认的命令，启动 main 文件
CMD ["./main", "--url=http://127.0.0.1:80"]
```
6. 最终的目录结构

```
E:\DOCKER
└─cangjie
    ├─runtime
    │  └─lib
    │      ├─linux_x86_64_llvm
    │      └─windows_x86_64_llvm
    └─tools
        ├─bin
        ├─config
        └─lib
```

7. 构建镜像

``` cmd
docker build -t api:1.0 .
docker run -it api:1.0
```