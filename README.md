
# **asp4cj** 

## 项目简介 📖

asp4cj框架是一个基于 **ASP.NET Core** 的开源项目，旨在为开发者提供高效、简洁的 Web 应用程序开发体验。该项目借鉴了 Microsoft 的技术栈，同时融合了许多创新的设计理念。

> **⚠️ 声明：** 本项目仅用于学习和测试，切勿用于生产环境。作者对因使用该框架导致的任何问题不承担责任。
>
> **📂 示例项目：** [quickstart](https://gitcode.com/soulsoft/asp_quickstart) 通过该项目的示例代码快速掌握 ASP 框架的使用。
>
> **👏 关于AOT：** 待语言强大稳定之后会全面接入aot
> 
> **授权协议：** 转载或二次开发时请注明出处即可：[soulsoft GitCode](https://gitcode.com/soulsoft/asp)
---

## 参与贡献 🤝

欢迎更多开发者参与贡献，包括提交代码、创意、建议，**基于asp4cj扩展**等！你可以通过以下途径加入我们的开发社区：

- **QQ群**：307564339
- **Issues**： [asp](https://gitcode.com/soulsoft/asp)
---

## 现有生态 👊

- **任务调度**：[scd](https://gitcode.com/soulsoft/scd)

## 引入依赖 📦

```toml
[dependencies]
    asp = { git = "https://gitcode.com/soulsoft/asp.git", branch = "main"}
```

---

## 开发文档 📚

1. [配置](./docs/configuration.md)
2. [日志](./docs/logging.md)
3. [依赖注入](./docs/injection.md)
4. [Docker 部署](./docs/docker.md)

---

## 目录结构 📂

``` cmd
├─docs
├─src
│  ├─extensions #扩展
│  │  ├─configuration #配置源
│  │  ├─injection #依赖注入
│  │  ├─logging #日志记录
│  │  ├─options #选项(规划中)
│  │  └─threading #线程
│  ├─hosting #主机
│  │  ├─builder #构建主机
│  │  ├─cors #跨域
│  │  ├─files #静态文件
│  │  ├─http #http协议
│  │  ├─mvc #MVC
│  │  └─routing #路由
│  └─macros #宏
```
---



## 依赖注入

``` cangjie
public class DbConnection {
    public init DbConnection(connectionString:String) {}
}
public class DbContext {
    public init DbContext(connection:DbConnection) {}
}
main () {
    let builder = WebApplication.createBuilder()
   
    //使用工厂模式注册，是不走反射的，你可以通过编写宏来构建工厂（asp也内置了一个宏来生成工厂）
    builder.servies.addScoped<DbConnection>{sp =>
        DbConnection(sp.configuration.getConnectionString("mysql").getOrThrow())
    }
    builder.servies.addScoped<DbContext, DbContext>()
    let app = builder.build()
    
    //创建作用域并解析作用域服务
    try(scope = app.services.createScope()) {
        let context = scope.services.getOrThrow<DbContext>()
    }
    app.run()
}
```

## 内置服务 🛠️

asp中内置了少量的服务，你可以按需解析，内置的服务使用的是工厂模式注册的，解析是不走反射的

| 中间件                                             | 作用                                         |
|---------------------------------------------------|----------------------------------------------|
| **IConfiguration**                                | 配置源                               |
| **ILoggerFactory**                                | 日志工厂                                 |
| **IHostEnvironment**                              | 环境信息                             |
| **IServiceProvider**                              | 容器                                 |
| **IHttpContextAccessor**                          | HttpContext访问器             |

---

## 配置选项

* asp为了统一用户的配置思维和习惯，引入了配置选项概念，并且支持命名选项用来处理多架构问题

``` cangjie
public class MvcOptions {
    public var scheme = "default"
    public var name = "ASP.NET Core"
    public var version = "1.0.0"
}

main (){
    let builder = WebApplication.createBuilder()
    
    //在所有的configure之后执行
    builder.options.configureAfter<MvcOptions>({ options => 
        options.version = "1.3.0"
    })

    builder.options.configure<MvcOptions>({ options => 
        options.version = "1.0.0"
    })

    builder.options.configure<MvcOptions>({ options => 
        options.version = "1.2.0"
    })
    let app = builder.build()
    let options = app.services.getOrThrow<IOptions<MvcOptions>>()
    println(options.value.version)//输出1.3.0
}
// 通过该案例可知使用configureAfter可以无视顺序（前提是框架内部调用的都是configure），修改框架的默认值。
```

* 为了处理多架构，还支持*命名选项*

``` cangjie
main (){
    let builder = WebApplication.createBuilder()

    builder.options.configure<MvcOptions>("s1",{ options => 
        options.scheme = "scheme1"
    })

    builder.options.configure<MvcOptions>("s2",{ options => 
        options.scheme = "scheme2"
    })

    let app = builder.build()
    let options = app.services.getOrThrow<IOptions<MvcOptions>>()
    println(options.value.scheme) //输出:default
    println(options.get("s1")) //输出:scheme1
    println(options.get("s2")) //输出:scheme2
}
```

* 如果你单纯的想放入容器中，不想配置

``` cangjie
main (){
    let builder = WebApplication.createBuilder()

    builder.options.addOptions<MvcOptions>()
    //你还可以将选项直接注入到容器
    builder.services.addSingleton<MvcOptions>{sp=>sp.getOrThrow<IOptions<MvcOptions>>().value}
    let app = builder.build()
    let options = app.services.getOrThrow<IOptions<MvcOptions>>()
    let mvcOptions = app.services.getOrThrow<MvcOptions>()
    println(options.value.scheme) //输出:default

}
```

## 快速启动 🚀

启动一个 WebHost：

```cangjie
import asp.hosting.builder.*

main(args :Array<String>) {
    let builder = WebApplication.createBuilder(args)
    let app = builder.build()
    app.run()
}
```

---

## 中间件 🔄

中间件用于处理请求管道中的逻辑，每个中间件负责执行单一职责。

### 处理跨域请求

```cangjie
// 配置跨域
app.useCors{ policy => 
    policy.allowAll()
}
```
### 健康检查

```cangjie
var builder = WebApplication.createBuilder(args)
builder.services.addHealthChecks()
    .addCheck("self"){
        HealthCheckResult.healthy()
    }
let app = builder.build()    
app.useHealthChecks("/health")    
```

### 自定义日志中间件

```cangjie
public class LoggingMiddleware <: IMiddleware {
    public func invoke(context: HttpContext, next: () -> Unit) {
        println(context.request.url)
        next()
    }
}

// 使用日志中间件
app.use<LoggingMiddleware>()
```

### 自定义异常处理

```cangjie
public class ExceptinHandlerMiddleware <: IMiddleware {
    private let _logFactory: ILoggerFactory

    public init(logfactory: ILoggerFactory) {
        _logFactory = logfactory
    }

    public func invoke(context: HttpContext, next: () -> Unit) {
        try {
            next()   
        }catch (ex: Exception) {
            let logger = _logFactory.createLogger("asp.hosting.lifetime")
            logger.error(ex.message, ex)
        }
    }
}
app.use<ExceptinHandlerMiddleware>()
```

### 内置的中间件

| 中间件                                             | 作用                                         |
|---------------------------------------------------|----------------------------------------------|
| **CorsRoutingMiddleware**                         | 处理跨域请求                                  |
| **EndpointRoutingMiddleware**                     | 查找与请求对应的 endpoint，放入 HttpContext   |
| **DefaultFilesMiddleware**                        | 处理根路径请求并返回默认文件                   |
| **StaticFilesMiddleware**                         | 处理静态文件请求                              |
| **HealthCheckMiddleware**                         | 复制执行健康检查                              |
| **EndpointMiddleware**                            | 执行 HttpContext 中的 Endpoint                |
| **NotFoundMiddleware(内置)**                      | 返回 404 错误                                 |
| **ServiceLifetimeMiddleware(内置)**               | 创建请求范围的作用域并注入服务                  |


---
## Mini-Api 🎛️

``` cangjie
import asp.hosting.builder.*

main(args :Array<String>) {
    let builder = WebApplication.createBuilder(args)
    let app = builder.build()
    app.mapEndpoints { endpoints =>
        endpoints.mapGet("/health") {
            context => context.responseBuilder.body("healthy")
        }
    }
    app.run()
}
```
## Controller 🎛️

ASP 支持基于 Controller-Action 的动态路由，支持依赖注入，支持 request-scope 生命周期。

```cangjie
@HttpRoute["/api/[controller]/[action]"]
public class TestController <: Controller {
    private let service: SomeService

    init(service: SomeService) {
        this.service = service
    }

    @HttpGet
    @HttpPut
    @HttpPost["create"]
    public func hello(): IActionResult {
        let res = HelloResult()
        res.name = "花间岛"
        res.id = 1024
        return json(res)
    }

    @HttpGet["/index"]
    public func index() {
        service.doSomething()
        context.responseBuilder.body("ednpoint:index")
    }
}

@Inject
public class SomeService <: Resource {
    let logger: ILogger
    let context: HttpContext

    init(accessor: IHttpContextAccessor, logFactory: ILoggerFactory) {
        context = accessor.context.getOrThrow()
        logger = logFactory.createLogger("asp.SomeService")
    }

    public func doSomething() {
        logger.info("working,request-path:${context.request.url}...")
    }

    public func isClosed() {
        false
    }

    public func close() {
        logger.warn("SomeService被释放了")
    }
}
```

---

## 后台任务 ⏳

通过实现IHostedService开启一个线程，来执行你的后台任务

```cangjie
// BackgroundService 的 execute 会帮你开线程，你无需额外开线程
public class AHostedService <: BackgroundService {
    private let _logFactory: ILoggerFactory
  
    public init(logFactory: ILoggerFactory) {
        _logFactory = logFactory
    }
  
    public func execute(cancellationToken: CancellationToken) {
        let logger = _logFactory.createLogger("TestHostedService")
        while (!cancellationToken.isCancellationRequested) {
            logger.info("A woring...")
            sleep(Duration.second * 3)
        }
    }
}

// 注册：注意是单例的，你可以注册多个后台任务
builder.addHostedService<BHostedService>()

// 开始调度后台任务
app.run()
```

---

## 总结 📝

ASP 框架为开发者提供了一个高效、简洁的 Web 应用程序开发体验。通过丰富的中间件、依赖注入、动态路由等功能，开发者可以快速构建出功能强大的 Web 应用。欢迎加入我们的社区，一起推动 ASP 框架的发展！ 🌟

---
